# win-installer
> 💽 NSIS 自定义安装包

### 安装界面

#### Step 1

<img src="./image/install-step-1.png" width="600">

#### Step 2

<img src="./image/install-step-2.png" width="600">

#### Step 3

<img src="./image/install-step-3.png" width="600">

#### Step 4

<img src="./image/install-step-4.png" width="600">

### 卸载界面

#### Step 1

<img src="./image/uninstall-step-1.png" width="600">

#### Step 2

<img src="./image/uninstall-step-2.png" width="600">

### LICENSE
> [Zlib](./LICENSE)
